<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace ProcessMaker\Services\Api\Halloo;

use \ProcessMaker\Services\Api;
use \Luracast\Restler\RestException;

/**
 * Halloo\HallooApi Api Controller
 *
 * @protected
 */
class HallooApi extends Api{
    
    /**
     *
     * @author gustavo.cruz@colosa.com
     * @return array
     *
     * @url GET /notes/
     */
    public function getNotes() {
        try {
            $notes = new \ProcessMaker\BusinessModel\Halloo\HallooNotes();
            $result = $notes->getNotes(array('userId' => $this->getUserId()));
            return $result;
        } catch (\Exception $e) {
            throw new RestException(Api::STAT_APP_EXCEPTION, $e->getMessage());
        }
    }

    /**
     *
     * @author gustavo.cruz@colosa.com
     * @return array
     *
     * @url GET /me/
     */
    public function getUser() {
        try {
            $user = new \ProcessMaker\BusinessModel\User();
            $result = $user->getUser($this->getUserId());
            return $result;
        } catch (\Exception $e) {
            throw new RestException(Api::STAT_APP_EXCEPTION, $e->getMessage());
        }
    }
}
