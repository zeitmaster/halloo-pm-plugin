<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace ProcessMaker\BusinessModel\Halloo;
/**
 * Description of HallooNotes
 *
 * @author gustav
 */
class HallooNotes {
    protected $case;

    public function __construct() {
        $this->case = new \ProcessMaker\BusinessModel\Cases ();
    }

    public function getNotes($params) {
        $cases = $this->case->getList($params);
        $notes = array();
        foreach ($cases['data'] as $case) {
            $filterArray = array();
            //$notes[] = $case['app_uid'];
            $caseNotes = $this->case->getCaseNotes($case['app_uid'], $params['userId'], $filterArray);
            foreach ($caseNotes['data'] as $key => $caseNote) {
                $caseNotes['data'][$key] = $this->processUserData($caseNote);
            }
            $notes = array_merge($notes, $caseNotes['data']);
        }
        return $notes;
    }
    
    public function processUserData($caseNote) {
        $user = new \ProcessMaker\BusinessModel\User();
        $noteUser = $user->getUser($caseNote['usr_uid']);
        $caseNote['username'] = $noteUser['usr_username'];
        $caseNote['firstname'] = $noteUser['usr_firstname'];
        $caseNote['lastname'] = $noteUser['usr_lastname'];
        $caseNote['email'] = $noteUser['usr_email'];
        $caseNote['uid'] = md5($caseNote['app_uid'].$caseNote['usr_uid'].$caseNote['note_date']);
        return $caseNote;
    }
}