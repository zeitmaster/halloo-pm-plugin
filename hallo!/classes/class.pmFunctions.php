<?php
/**
 * class.hallo!.pmFunctions.php
 *
 * ProcessMaker Open Source Edition
 * Copyright (C) 2004 - 2008 Colosa Inc.
 * *
 */

////////////////////////////////////////////////////
// hallo! PM Functions
//
// Copyright (C) 2007 COLOSA
//
// License: LGPL, see LICENSE
////////////////////////////////////////////////////

function hallo!_getMyCurrentDate()
{
	return G::CurDate('Y-m-d');
}

function hallo!_getMyCurrentTime()
{
	return G::CurDate('H:i:s');
}
